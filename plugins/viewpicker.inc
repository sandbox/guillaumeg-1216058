<?php
/**
 * Specially named implementation of hook_wysiwyg_plugin().
 *
 * Should be named {$module}_{$plugin}_plugin().
 */

function tinymce_view_picker_viewpicker_plugin() {

	$plugins['viewpicker'] = array(
    'title' => t('View picker'),    
    'icon file' => 'viewpicker_icon.png',
    'icon title' => t('Insert a view'),
    'settings' => array(
    	'dialog' => array(
    		'title' => t('Insert a view'),
				'url' => url('admin/tinymce-view-picker'),
				'width' => 640,
				'height' => 500,
	),
	),
	);
	return $plugins;
}
