Drupal.behaviors.tinyMCEViewPicker = function (context) {
	
  var parentWindow = (window.opener) ? window.opener : window.parent;
  if (parentWindow && parentWindow.Drupal) {
    var instanceId = parentWindow.Drupal.wysiwyg.activeId;
    var editor = parentWindow.Drupal.wysiwyg.instances[instanceId].editor;
    var content = '';
    var selection;

    // Fetch text selection, different for different editors.
    switch(editor) {
      case "tinymce":
        content = parentWindow.tinyMCE.activeEditor.selection.getContent();
        selection = parentWindow.tinyMCE.activeEditor.selection;
      break;
    }
    
    // Strip anchors from content.
    content = content.replace(/<[//]{0,1}(A|a)[^><]*>/g, "");

    // Capture click on "Insert link".
    $(".sticky-enabled a").click(function() {
    	
      // Insert content.
      if (parentWindow && parentWindow.Drupal) {
        var title = $(this).parent().parent().children("td:eq(0)").text().replace(/^\s+|\s+$/g, "");
        
        parentWindow.Drupal.wysiwyg.instances[instanceId].insert('[view:'+title+']');
      
        parentWindow.Drupal.wysiwyg.instances[instanceId].closeDialog(window);
      }
      else {
        alert(Drupal.t("The view cannot be inserted because the parent window cannot be found."));
      }
      return false;
    });
  }
};
